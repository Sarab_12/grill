"""grill URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from os import stat
from django.contrib import admin
from django.http.request import validate_host
from django.urls import path
from django.urls.conf import include
from grillapp import views

from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.home,name='home'),
    path('about/',views.about,name='about'),
    path('product/',views.product,name='product'),
    path('contact/',views.contact_us,name='contact'),
    path('signin/',views.signin,name='signin'),
    path('signup/',views.signUp,name='signup'),
    path('singlePost/',views.singlePost,name='singlePost'),
    path('dashboard/',views.dashboard,name='dashboard'),
    path('cart/',views.cart,name='mycart'),


    path('contact/',views.contact_us,name='contact'),
    path('signin/',views.signin,name='signin'),
    path('signup/',views.signUp,name='signup'),
    path('singlePost/',views.singlePost,name='singlePost'),
    path('logout/',views.sign_out,name='logout'),
    path('add_to_cart/',views.add_to_cart,name='add_to_cart'),
    path('change_quantity/',views.change_quantity,name='change_quantity'),
    path('check_username_exists/',views.check_username_exists,name='check_username_exists'),

    path('add_to_cart/', views.add_to_cart, name="add_to_cart"),
    path('change_quantity/', views.change_quantity, name="change_quantity"),

    path('paypal/', include('paypal.standard.ipn.urls')),
    path('payment_success/', views.payment_success, name='payment_success'),
    path('payment_failed/', views.payment_failed, name='payment_failed'),
    path('forgotPassword/',views.forgotPassword, name='forgotPassword'),
    path('resetPassword/',views.resetPassword,name='resetPassword'),
]+static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
