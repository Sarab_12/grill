from django.contrib import admin
from grillapp.models import (Contact_us,Team, Category, Item,ProfileTable,
Cart, Order )
# Register your models here.

admin.site.site_header='Grill | Admin'

class Contact_usAdmin(admin.ModelAdmin):
    #fields=['name','email','message']
    list_display=['id','name','subject','added_on','updated_on']
    list_editable=['name']
    list_filter=['name','added_on']
    search_fields=['subject']

class TeamAdmin(admin.ModelAdmin):
    list_display=['name','designation','picture']
   
class CategoryAdmin(admin.ModelAdmin):
    list_display=['name','description','image']

class ItemAdmin(admin.ModelAdmin):
    list_display=['name','image','ingredients','details','category','price','discounted_price']
    
class CartAdmin(admin.ModelAdmin):
    list_display=['id','user','quantity']

class OrderAdmin(admin.ModelAdmin):
    list_display = ['id', 'user_id', 'address', 'status']

admin.site.register(Contact_us,Contact_usAdmin)
admin.site.register(Team,TeamAdmin)
admin.site.register(Category,CategoryAdmin)
admin.site.register(Item,ItemAdmin)
admin.site.register(ProfileTable)
admin.site.register(Cart,CartAdmin)
admin.site.register(Order,OrderAdmin)
