# Generated by Django 3.0.8 on 2021-11-18 07:49

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('grillapp', '0006_auto_20211110_1244'),
    ]

    operations = [
        migrations.CreateModel(
            name='Cart',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.IntegerField(default=1)),
                ('added_on', models.DateTimeField(auto_now_add=True)),
                ('product', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='grillapp.Item')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='grillapp.ProfileTable')),
            ],
            options={
                'verbose_name_plural': 'Cart Table',
            },
        ),
    ]
