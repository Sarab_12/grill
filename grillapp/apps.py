from django.apps import AppConfig


class GrillappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'grillapp'
