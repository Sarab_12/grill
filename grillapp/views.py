from os import name
from django.core.mail import send_mail
from django.core.checks import messages
from django.http.response import JsonResponse
from django.shortcuts import render, reverse
from django.http import HttpResponse, HttpResponseRedirect
from grillapp.models import Category, Contact_us,Team,Item, ProfileTable, Cart, Order
from django.contrib.auth.models import User 
from django.contrib.auth import login, logout, authenticate
from paypal.standard.forms import PayPalPaymentsForm
from django.conf import settings

def home(request):
    context={}
    cart_items  = Cart.objects.filter(user__user__username=request.user.username)
    context['cart_items'] = cart_items
    recent_dishes = Item.objects.all().order_by('-id')[:6]
    context['recent_dishes']=recent_dishes
    return render(request,"home.html",context )

def about(request):
    context={}
    cart_items  = Cart.objects.filter(user__user__username=request.user.username)
    context['cart_items'] = cart_items
    context['team']=Team.objects.all().order_by('name')
    context['categories'] = Category.objects.all().order_by('name')

    query = request.GET.get('q')
    if query==None or query=='all':
        context['items'] = Item.objects.all().order_by('name')
    else:
        context['items'] = Item.objects.filter(category__id=query).order_by('name')
    return render(request,"about.html",context)

def product(request):
    context={}
    cart_items  = Cart.objects.filter(user__user__username=request.user.username)
    context['cart_items'] = cart_items
    context['categories'] = Category.objects.all().order_by('name')
    
    query = request.GET.get('q')
    if query==None or query=='all':
        context['items'] = Item.objects.all().order_by('name')
    else:
        context['items'] = Item.objects.filter(category__id=query).order_by('name')
    
    if "query" in request.GET:
        txt = request.GET.get('query')
        context['items'] = Item.objects.filter(name__contains = txt)

    return render(request,"product.html", context)   

def cart(request):
    context = {}
    if request.user.is_authenticated:
        user=User.objects.get(username=request.user.username)
        if request.user.is_superuser or request.user.is_staff:
            return HttpResponseRedirect('/admin') 
    else:
        return HttpResponseRedirect('/signin')       
    user_profile = ProfileTable.objects.get(user__username=request.user.username)
    context['profile'] = user_profile
    if "delete" in request.GET:
        id = request.GET.get('delete')
        Cart.objects.get(id=id).delete()
        return HttpResponseRedirect('/cart')
    if "remove" in request.GET:
        user_id = request.GET.get('remove')
        cart = Cart.objects.filter(user__user__id=user_id)
        for i in cart:
            i.delete()
    
    cart_items  = Cart.objects.filter(user__user__username=request.user.username)
    
    context['cart_items'] = cart_items

    final = []
    for i in cart_items:
        item = {
            'price':i.product.price * i.quantity,
            'discounted_price':i.product.discounted_price * i.quantity
        }
        final.append(item)

    if "get_json_items" in request.GET:
        return JsonResponse({'items':final})
   
    names = ""
    ids = ""
    for cobj in cart_items:
        ids+= str(cobj.product.id)+",";
        names+= str(cobj.product.name)+",";

    paypal_dict = {
        'business':settings.PAYPAL_RECEIVER_EMAIL,'names':names,
        'notify_url':'http://{}{}'.format(settings.HOST, reverse('paypal-ipn')),
        'return_url':'http://{}{}'.format(settings.HOST, reverse('payment_success')),
        'cancel_url':'http://{}{}'.format(settings.HOST, reverse('payment_failed')),
    }   
    context['paypal_dict'] = paypal_dict

    if "place_order" in request.GET:
        order_obj = Order(user_id=user_profile, product_ids=ids, address=user_profile.address)
        order_obj.save()
        order_obj.invoice_id = "INV-"+str(order_obj.id)
        order_obj.save()
        request.session['order_id'] = order_obj.id 
        return JsonResponse({"status":"success"})
        
    return render(request,"cart.html", context) 

def payment_success(request):
    oid = request.session.get('order_id')
    order_obj = Order.objects.get(id=oid)
    order_obj.status = True 
    order_obj.save()
    
    data = Cart.objects.filter(user__user__username=request.user.username)
    for i in data:
        i.delete()
    return render(request,"payment_Successfull.html")  

def payment_failed(request):
    return render(request,"payment_failed.html")

def contact_us(request):
    context={}
    if request.method=="POST":
        if request.user.is_authenticated:
            user=User.objects.get(username=request.user.username)
            name=request.POST.get('name')
            email=request.POST.get('email')
            subject=request.POST.get('subject')
            message=request.POST.get('text')

            obj=Contact_us.objects.create(name=name,email=email,subject=subject,message=message)
            obj.save()
            context['message']=f'{name} thanks for feedback!'
        else:
            HttpResponseRedirect('/signin')  
   
    cart_items  = Cart.objects.filter(user__user__username=request.user.username)
    context['cart_items'] = cart_items
    return render(request,"contact-us.html",context)  

def signin(request):
    context={}

    if request.method=="POST":
        email = request.POST.get('email')   
        passw = request.POST.get('pass')   
        current_user = authenticate(username=email, password=passw)
        
        if current_user:
            login(request, current_user)
            if current_user.is_superuser == True:
                return HttpResponseRedirect('/admin')   
            if current_user.is_active == True:
               return HttpResponseRedirect('/dashboard')   
        else:
            context['message'] = 'Invalid Login Details' 
    return render(request,"signin.html", context)  

def signUp(request):
    context={}
    if request.method=="POST":
        name=request.POST.get('name')
        email=request.POST.get('email')
        password=request.POST.get('pass')
        
        check = User.objects.filter(username=email)
        if len(check) == 0:
            usr = User.objects.create_user(email, email, password)
            usr.first_name = name 
            usr.save()
            profile = ProfileTable(user=usr)
            profile.save()
            context['status'] = f'{name} Registered successfully!'
        else:
            context['error'] = f'A user with this email already exists!'    
            
    return render(request,"signUp.html", context)  

def singlePost(request):
    context={}
    cart_items  = Cart.objects.filter(user__user__username=request.user.username)
    
    context['cart_items'] = cart_items
    return render(request,"single-post.html")

def dashboard(request):
    context={}
    cart_items  = Cart.objects.filter(user__user__username=request.user.username)
    context['cart_items'] = cart_items
    order_history = Order.objects.filter(user_id__user__username = request.user.username).order_by('-id')
    data =[]
    for ord in order_history:
        product_ids = ord.product_ids[:-1].split(',');
        products = Item.objects.filter(id__in = product_ids)
        o = {
            'id':ord.id,
            'ordered_on':ord.ordered_on,
            'status':ord.status,
            'invoice':ord.invoice_id,
            'products':products,
        }
        data.append(o)
    context['order_history'] = data
    if request.user.is_authenticated:
        user=User.objects.get(username=request.user.username)
        if request.user.is_superuser or request.user.is_staff:
            return HttpResponseRedirect('/admin')         
        user_profile = ProfileTable.objects.get(user__username=request.user.username)
        context['profile'] = user_profile
        if 'change_password' in request.POST:
            old_pass=request.POST.get('old_password')
            new_pass=request.POST.get('new_password')

            is_matched=user.check_password(old_pass)
            if is_matched==True:
                 user.set_password(new_pass)
                 user.save()
                 login(request,user)
                 context['message']="Password changes succesfully!"
                 
            else:
                context['error']="Your Current Password Is Incorrect!"       
        if 'edit_profile' in request.POST:
             name=request.POST.get('name')
             email=request.POST.get('email')
             contact=request.POST.get('phone')
             address=request.POST.get('address')
             image=request.FILES.get('profile_pic')
             if image!=None:
                user_profile.profile_pic=image
                

             user_profile.user.first_name=name
             user_profile.user.username=email
             user_profile.user.save()
             user_profile.contact_number=contact
             user_profile.address=address
             user_profile.save()
             context['message']="changes added successfullly!"
        
        if 'delete_order' in request.GET:
            order_id = request.GET.get('delete_order')
            print("order_id", order_id)
            Order.objects.get(id=order_id).delete()
            context['message'] = "Order deleted successfully!"
    else:
        return HttpResponseRedirect('/')
    return render(request,"dashboard.html", context)
      
def check_username_exists(request):
    usern = request.GET.get('username')

    query = len(User.objects.filter(username=usern))
    if query == 0:
        return JsonResponse({'status':False,'message':"User Doesn't exists!"})
    else:
        return JsonResponse({'status':True,'message':"User with this email already exists!"})

def sign_out(request):
    logout(request)
    return HttpResponseRedirect('/')


def add_to_cart(request):
    user_id = request.user.id 
    product_id = request.GET.get('product_id')

    is_exist = Cart.objects.filter(user__user__id=user_id, product__id=product_id)
    
    if len(is_exist) == 0:
        user = ProfileTable.objects.get(user__id=user_id)
        product = Item.objects.get(id=product_id)
        
        cart_obj = Cart(user=user, product=product, quantity=1)
        cart_obj.save()
        return JsonResponse({'status':1, 'message':'Item added!'})
    else:
        return JsonResponse({'status':0, 'message':'Item already exists in your cart!'})

def change_quantity(request):
    operation_to_perform = request.GET.get('operation')
    cart_item_id = request.GET.get('cart_item')
    n_cart_items  = len(Cart.objects.filter(user__user__username=request.user.username))

    c_obj = Cart.objects.get(id=cart_item_id)
    
    if operation_to_perform == "plus":
        c_obj.quantity = c_obj.quantity + 1
    
    elif operation_to_perform == "minus":
        c_obj.quantity = c_obj.quantity - 1
    
    c_obj.save()
    quantity = c_obj.quantity
    
    if c_obj.quantity == 0:
        Cart.objects.get(id=cart_item_id).delete()
        quantity = 0
    
    return JsonResponse({'quantity':quantity,'n_cart_items':n_cart_items})

def forgotPassword(request):
    return render(request,"forgot_Password.html")

import random    
def resetPassword(request):
    print("data==",request.GET)
    pass