from django.db import models
from django.contrib.auth.models import User

# Create your models here.

class Team(models.Model):
    name=models.CharField(max_length=200)
    designation=models.CharField(max_length=200)
    picture=models.ImageField(upload_to='team/%Y/%m/%d')
    facebook_profile=models.URLField(default="https://www.facebook.com/")        
    linkedin_profile=models.URLField(default="https://www.linkedin.com/")        
    twitter_profile=models.URLField(default="https://www.twitter.com/")    
    name = models.CharField(max_length=200)
    designation = models.CharField(max_length=200)
    picture = models.ImageField(upload_to='team/%Y/%m/%d')
    facebook_profile = models.URLField(default = "https://www.facebook.com/")
    linkedin_profile = models.URLField(default = "https://www.linkedin.com/")
    twitter_profile = models.URLField(default = "https://twitter.com/")

    def __str__(self):
        return f"{self.name} ({self.designation})"

class Category(models.Model):
    name=models.CharField(max_length=200,unique=True)
    description=models.TextField(blank=True)
    image=models.ImageField(upload_to='categories')
    added_on=models.DateTimeField(auto_now_add=True)
    updated_on=models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    class Meta:
        verbose_name_plural = 'Category Table'

class Item(models.Model):
    name=models.CharField(max_length=200,unique=True)
    image=models.ImageField(upload_to='dishes/%Y/%m/%d')
    ingredients=models.TextField()
    details=models.TextField(blank=True)
    category=models.ForeignKey(Category,on_delete=models.CASCADE)
    price=models.FloatField()
    discounted_price=models.FloatField(blank=True)
    is_available=models.BooleanField(default=True)
    added_on=models.DateTimeField(auto_now_add=True)
    updated_on=models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name_plural='Dish Table'

    def __str__(self):
        return self.name

class Item(models.Model):
    name = models.CharField(max_length=200, unique=True)
    image = models.ImageField(upload_to='dishes/%Y/%m/%d')
    ingredients = models.TextField()
    details = models.TextField(blank=True)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    price = models.FloatField()
    discounted_price = models.FloatField(blank=True)
    is_available = models.BooleanField(default=True)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name 

    class Meta:
        verbose_name_plural ="Dish Table"
    

class ProfileTable(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    profile_pic = models.ImageField(upload_to='profiles/%Y/%m/%d', default='no-user.jpg')
    contact_number = models.CharField(max_length=15, blank=True)
    address = models.TextField(blank=True)
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)
 
    def __str__(self):
        return self.user.first_name

    class Meta:
        verbose_name_plural = "Profile Table"

class Contact_us(models.Model):
    user=models.ForeignKey(ProfileTable,on_delete=models.CASCADE,null=True)
    name = models.CharField(max_length=100)
    email = models.EmailField()
    subject = models.CharField(max_length=250)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural='Contact Table'

class Cart(models.Model):
    user=models.ForeignKey(ProfileTable,on_delete=models.CASCADE)
    product=models.ForeignKey(Item,on_delete=models.CASCADE)
    quantity=models.IntegerField(default=1)
    added_on=models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user.user.username

    class Meta:
        verbose_name_plural ="Cart Table"

class Order(models.Model):
    user_id = models.ForeignKey(ProfileTable, on_delete=models.CASCADE)
    product_ids = models.CharField(max_length=500)
    invoice_id = models.CharField(max_length=300)
    address = models.TextField()
    status = models.BooleanField(default=False)
    ordered_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.user_id.user.username

    class Meta:
        verbose_name_plural = "Order Table"
    
